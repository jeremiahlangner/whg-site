(function() {
    let throttle = function(type, name, obj) {
        obj = obj || window;
        let running = false;
        let func = function() {
            if (running) { return; }
            running = true;
             requestAnimationFrame(function() {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });
        };
        obj.addEventListener(type, func);
    }; 
    throttle("resize", "optimizedResize");
})();

/* Libary for Sequencing CSS animations. */
(function() {
	let interactiveObject = function(elementSelector, animations) {
		let scope = this;

		this.elementSelector = elementSelector;
		this.animations = animations;

		this.element = document.querySelector(this.elementSelector);
		this.transitioning = false;
		this.activeState = '';
		this.activeTimers = [];
		this.animationQueue = [];
		this.looping = '';

		this.getTransformOrigin = function() {
			let coords = this.element.getBoundingClientRect(); 
			let origin = [ coords.left + (coords.width/2), coords.top + (coords.height/2) ];
			return origin;
		};

		this.getElementTransform = function() {
			let currentTransformStyle = window.getComputedStyle(this.element, null).transform;
			let values = currentTransformStyle.split('(')[1].split(')')[0].split(',');
			
			let a = parseFloat(values[0]);
			let b = parseFloat(values[1]);
			// let c = values[2];
			// let d = values[3];
			// let tx = values[4];
			// let ty = values[5];
			// let sin = values[1]/scale;

			let scale = Math.sqrt(a*a + b*b);
			let angle = Math.round(Math.atan2(b, a) * (180/Math.PI));

			return [scale, angle, parseFloat(values[4]), parseFloat(values[5])];
		};
	
		this.animate = function() {	
			if(this.animationQueue.length > 0 && this.activeState == '') {
				this.activeState = this.animationQueue[0];
				
				let animation;

				for(let i = this.animations.length-1; i >= 0; i--) {
					if(this.animationQueue[0] == animations[i].name) {
						animation = animations[i];
						break;
					}
				}

				let animationDefaults = {
					name: '',
					time: 100, 
					func: 'linear', 
					rotation: 0,
					scale: 1,
					position: [0,0],
					repeat: 1
				}

				if(!animation.time) {
					animation.time = animationDefaults.time;
				}

				if(!animation.func) {
					animation.func = animationDefaults.func;
				}
				
				if(!animation.rotation) {
					animation.rotation = animationDefaults.rotation;
				}

				if(!animation.scale) {
					animation.scale = animationDefaults.scale;
				}

				if(!animation.position) {
					animation.position = animationDefaults.position;
				}

				if(!animation.repeat) {
					animation.repeat = animationDefaults.repeat;
				}

				switch (animation.type) {
					case 'sequentialAdditive':
						if(!animation.sequence) {
							console.log('Can\'t play animation: No sequence defined.');
						} else {
							if(animation.repeat && animation.repeat > 0) {
								for(let i = animation.repeat; i > 0; i--) {
									for(let item of animation.sequence) {
										this.queueAnimation(item);
									}
								}
								this.animationQueue.shift();
								this.activeState = '';
								this.animate();
							} else if (animation.repeat && animation.repeat < 0) {
								this.looping = this.animationQueue[0];

								for(let item of animation.sequence) {
									this.queueAnimation(item);
								}

								this.animationQueue.shift();
								this.activeState = '';
								this.animate();
							}
						}
						break;
					case 'parallel':
						if(!animation.sequence) {
							console.log('Can\'t play animation: No sequence defined.');
						} else {
							let joinedAnimations = [];
							let geometry = this.getElementTransform();
							let origin = this.getRelativeTransformOrigin();
							
							let finalAnimation = {
								time: 100, 
								func: 'linear', 
								rotation: 0,
								scale: 1,
								position: [0,0],
								repeat: 1
							};

							if(animation.time) {
								finalAnimation.time = animation.time;
							}

							if(animation.func) {
								finalAnimation.func = animation.func;
							}

							for(let step of animation.sequence) {
								for(let anim of animations) {
									if(anim.name == step) {
										joinedAnimations.push(anim);
									}
								}
							}

							for(let anim of joinedAnimations) {
								anim.rotation ? finalAnimation.rotation += anim.rotation : finalAnimation.rotation = finalAnimation.rotation;
								
								anim.scale ? finalAnimation.scale = anim.scale * finalAnimation.scale : finalAnimation.scale = finalAnimation.scale;

								if(anim.position) {	
									finalAnimation.position[0] += anim.position[0];
									finalAnimation.position[1] += anim.position[1];
								}
							}

							this.animationQueue.shift();

							this.element.style.transition = 'all ' + finalAnimation.time + 'ms ' + finalAnimation.func;

							this.element.style.transform = 'scale(' + (geometry[0] * finalAnimation.scale) + ') ' + 'rotate(' + (geometry[1] + finalAnimation.rotation) + 'deg) translateX(' + (geometry[2] + finalAnimation.position[0]) + 'px) translateY(' + (geometry[3] + finalAnimation.position[1]) + 'px)';
						}

						if(this.animationQueue.length >= 0) {
							let timer = window.setTimeout(function() {
								scope.activeState = '';
								scope.animate();
							}, animation.time);

							this.activeTimers.push(timer);
						}
						if(this.animationQueue.length == 0) {
							if(this.looping != '') {
								this.animationQueue.push(this.looping);
								this.looping = '';
								this.animate();
							}
						}
						break;

					case undefined:
						/* Additive */
						let geometry = this.getElementTransform();
						let origin = this.getTransformOrigin();

						this.animationQueue.shift();

						this.element.style.transition = 'all ' + animation.time + 'ms ' + animation.func;

						this.element.style.transform = 'scale(' + (geometry[0] * animation.scale) + ') ' + 'rotate(' + (geometry[1] + animation.rotation) + 'deg) translateX(' + (geometry[2] + animation.position[0]) + 'px) translateY(' + (geometry[3] + animation.position[1]) + 'px)';
						
						if(this.animationQueue.length >= 0) {
							let timer = window.setTimeout(function() {
								scope.activeState = '';
								scope.animate();
							}, animation.time);

							this.activeTimers.push(timer);
						} 
						if(this.animationQueue.length == 0) {
							if(this.looping != '') {
								this.queueAnimation(this.looping);
								this.looping = '';
								this.animate();
							}
						}
						
						break;
				}
			}
		};

		this.clearQueue = function() {
			this.animationQueue = [];
		};

		this.resetState = function() {
			this.activeState = 'reset';
			this.clearQueue();
			for(timer of this.activeTimers) {
				clearTimeout(timer);
			}

			this.element.style.animation = '';
			this.element.style.transform = 'scale(1) rotate(0) translateX(0) translateY(0)';
			this.element.style.transition = 'all cubic-bezier(0.68, -0.55, 0.265, 1.55) 200ms';

			setTimeout(function() {
				scope.activeState = '';
			}, 200);
		};

		this.queueAnimation = function(animationToAdd) {
			for(let animation of this.animations) {
				if (animation.name == animationToAdd) {
					this.animationQueue.push(animationToAdd);
					
					if(scope.activeState == '') {
						scope.animate();
					}
				}
			}
		};

	};

	let adjustOrbit = function() {
		let orbit = document.querySelector('.orbit');
		let randomAngle = Math.random() * .5;
		orbit.style.transform = 'scale(0.995) rotate(' + randomAngle + 'deg)';
	};

	let resetOrbit = function() {
		let orbit = document.querySelector('.orbit');
		orbit.style.transition = 'transform cubic-bezier(0.175, 0.885, 0.5, 1.1) 1.3s';
		orbit.style.transform = 'scale(1) rotate(0deg)';	
	}

	let handleEvents = function() {
		/* Reset animations after display state. */
		setTimeout(function() {
			for(let interactiveObject of interactiveObjects) {
				interactiveObject.resetState();
			}
		}, 1000);

		/* Handle Click Events */
		document.addEventListener('click', function(event) {
			 if(event.target.getAttribute('data-action') != null) {
		        actions[event.target.getAttribute('data-action')](event);		
		    }
		});

		/* Handle Hover events */
		for(let interactiveObject of interactiveObjects) {
			interactiveObject.element.addEventListener('mouseenter', function(event) {
				let animationString = 'grow hover';
				let animations = animationString.split(' ');
				let overlay = document.querySelector('#overlayImage');
				overlay.style.transformOrigin = event.clientX + 'px ' + event.clientY + 'px';

				for(let animation of animations) {
					interactiveObject.queueAnimation(animation);
				}

				adjustOrbit();
			});

			interactiveObject.element.addEventListener('mouseleave', function() {
				let animationString = 'hover'
				let animations = animationString.split(' ');

				interactiveObject.resetState();
				resetOrbit();
			});
		}

		/* Handle Resize Events */
		window.addEventListener("optimizedResize", function() {	
			let mainBg = document.querySelector('.main-area');

		    mainBg.style.width = mainBg.getBoundingClientRect().height + 'px';
		});
	}

	let animations = [
		{
			name: 'grow',
			time: 300,
			scale: 1.3,
			func: 'cubic-bezier(0.68, -0.55, 0.265, 1.55)'
		},
		{
			name: 'saw',
			time: 600,
			rotation: -1,
			func: 'linear'
		},
		{
			name: 'see',
			time: 600,
			rotation: 1,
			func: 'linear'
		},
		{
			name: 'hover',
			type: 'sequentialAdditive',
			sequence: [
				'see',
				'saw',
				'saw',
				'see'
			],
			repeat: -1
		},
		{
			name: 'shrink',
			time: 400,
			scale: 0.9, 
			func: 'linear'
		}
	]

	let data = [ 
		{
			selector: '#social-layer',
			animations: animations
		},
		{
			selector: '#address-layer',
			animations: animations
		},
		{
			selector: '#ip-address-layer',
			animations: animations
		},
		{
			selector: '#email-layer',
			animations: animations
		},
		{
			selector: '#phone-layer',
			animations: animations
		},
		{
			selector: '#tv-layer',
			animations: animations
		}
	];

	let interactiveObjects = [];

	for(let item of data) {
		let interactiveObj = new interactiveObject(item.selector, item.animations);
		interactiveObjects.push(interactiveObj);
	}

	let openOverlay = function(event) {
		let overlaySource = event.target.getAttribute('data-overlayImage');
		let overlay = document.querySelector('#overlayImage');
		let paragraph = document.querySelector('#block-yui_3_17_2_1_1539651816078_6596');
		let paragraphBlock = event.target.getAttribute('data-text');
		let textSource = document.querySelector(paragraphBlock);
		
		overlay.src = overlaySource;
		overlay.style.transform = 'scale(1.0)';
		paragraph.innerHTML = textSource.innerHTML + '<a id="back-link" href="#">Back</a>';
		document.querySelector('#back-link').addEventListener('click', closeOverlay, false);

		document.querySelector('#overlay').style.opacity = '1.0';
		document.querySelector('#overlay').style.zIndex = '999999';
	};

	let closeOverlay = function(event) {
		document.querySelector('#overlay').style.opacity = '0';
		document.querySelector('#overlay').style.zIndex = '1';		
		let paragraph = document.querySelector('#block-yui_3_17_2_1_1539651816078_6596');

		let overlay = document.querySelector('#overlayImage');		
		overlay.style.transform = 'scale(0)';
		paragraph.innerHTML = mainParagraphHTML;
	};

	let navigate = function(event) {
		let url = event.target.getAttribute('navigation');

		location.href = (url);
	};

	/* Main Program Actions */
	let actions = {
		navigate: navigate,
		openOverlay: openOverlay,
		closeOverlay: closeOverlay
	};

	let mainParagraphHTML = document.querySelector('#block-yui_3_17_2_1_1539651816078_6596').innerHTML;
	/* Main Function calls */
	handleEvents();
})();